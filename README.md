# Code Names

Jeu Fiesta De Los Muertos en ligne avec Django et React.js

## Lancement

### Développement

La configuration Docker de développement fait appel à une base de données SQLite locale
pour plus de simplicité. Une configuration Docker Compose est fournie pour lancer l'application
de développement avec son serveur Redis dédié :
Pour lancer docker simplement en local, on pourra lancer en parallèle un "docker deamon" avec
```bash
dockerd
```

```bash
docker-compose up
```
Le serveur se lance alors par défaut sur le port 65000 de la machine.

### Production

3 images Docker de production sont incluses dans le dossier `docker/` selon le type de système de
gestion de bases de données à utiliser entre :
- SQLite
- MySQL
- PostgreSQL

La configuration de connexion des conteneurs de production passe par des variables d'environnement :
```
## Communes à toutes les images
FIESTA_HOST  # Nom de domaine autorisé pour l'application (known_hosts dans les settings Django)
REDIS_HOST      # Hôte du serveur Redis à utiliser
REDIS_PORT      # Port du serveur Redis à utiliser

## Pour l'image PostgreSQL
POSTGRES_HOST      # Hôte du serveur PostgreSQL à utiliser
POSTGRES_PORT      # Port du serveur PostgreSQL à utiliser
POSTGRES_DB        # Nom de la base de données de l'application
POSTGRES_USER      # Nom d'utilisateur PostgreSQL
POSTGRES_PASSWORD  # Mot de passe PostgreSQL

## Pour l'image MySQL
MYSQL_CNF  # Chemin vers le fichier de configuration MySQL à utiliser (à monter comme volume dans le conteneur)
```

Les images Docker de production sont disponibles publiquement dans leur dernière version sous les tags suivants :
```
## Images x86
maxenceblomme/fiesta-dlm:sqlite
maxenceblomme/fiesta-dlm:postgres
maxenceblomme/fiesta-dlm:mysql

## Images ARM
maxenceblomme/fiesta-dlm:sqlite-arm
maxenceblomme/fiesta-dlm:postgres-arm
maxenceblomme/fiesta-dlm:mysql-arm
```

## Mots jouables

La base de données créée par le projet est de base vide. Il faut donc y insérer les
noms de personnages jouables ainsi que les contraintes pour que l'application fonctionne.
Le script Python `populate_database.py` permet de la peupler automatiquement.

Ce script prend en argument le chemin vers le fichier contenant les personnages, ainsi que le chemin vers le fichier
contenant les contraintes, chaque élément devant être séparé des autres par un retour à la ligne.

Un troisième argument optionnel permet de dire au script de peupler une base SQLite ou PostgreSQL au lieu d'une
base MySQL. Pour cela, il suffit de rajouter un second argument :
```bash
python populate_database.py personnages contraintes 1  # Utiliser SQLite
python populate_database.py personnages contraintes 2  # Utiliser PostgreSQL
```

Le fichier `characters` contient 167 personnages, et le fichier constraints contient 11 contraintes. Ils peuvent etre utilisés pour
peupler la base de personnages et contraintes du jeu. On peut donc exécuter la commande suivante pour le faire :
```bash
python populate_database.py characters constraints {,0,1,2}
```
