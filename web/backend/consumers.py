﻿import json

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncWebsocketConsumer

from backend.consumer_requests.add_word import AddWordConsumer
from backend.consumer_requests.get_constraints import GetConstraintsConsumer
from backend.consumer_requests.get_results import GetResultsConsumer
from backend.consumer_requests.get_skulls import GetSkullsConsumer
from backend.consumer_requests.pick_new_character import PickNewCharacterConsumer
from backend.consumer_requests.reconnect_player import ReconnectPlayerConsumer
from backend.consumer_requests.register_player import RegisterPlayerConsumer
from backend.consumer_requests.request_types import RequestTypeEnum

from backend.consumer_requests.get_players import GetPlayersConsumer
from backend.consumer_requests.remove_player import RemovePlayerConsumer
from backend.consumer_requests.reset import ResetConsumer
from backend.consumer_requests.send_guesses import SendGuessesConsumer
from backend.consumer_requests.set_constraint_amount import SetConstraintAmountConsumer
from backend.consumer_requests.start_game import StartGameConsumer
from backend.utils.game_manager import get_state
from backend.utils.game_states import GameStateEnum

from backend.utils.player_manager import remove_player
from backend.utils.player_utils import set_player_connected, player_exists


class BackendConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.game_id = self.scope['url_route']['kwargs']['game_id']
        self.game_group_name = 'game_%s' % self.game_id

        self.player_name = ''

        # Join room group
        await self.channel_layer.group_add(
            self.game_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        if await database_sync_to_async(player_exists)(self.game_id, self.player_name):
            state = await database_sync_to_async(get_state)(self.game_id)
            if state == GameStateEnum.LOBBY:
                await database_sync_to_async(remove_player)(self.game_id, self.player_name)
            else:
                await database_sync_to_async(set_player_connected)(self.game_id, self.player_name, False)

        # Send message to room group
        await self.channel_layer.group_send(
            self.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.REMOVE_PLAYER.value,
                'name': self.player_name
            }
        )

        # Leave room group
        await self.channel_layer.group_discard(
            self.game_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        request_type = text_data_json['type']
        if not self.validate_call(text_data_json):
            await self.send(text_data=json.dumps({
                'type': request_type,
                'error': 1
            }))
            return
        elif request_type == RequestTypeEnum.RESET.value:
            await ResetConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.REGISTER_PLAYER.value:
            await RegisterPlayerConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_PLAYERS.value:
            await GetPlayersConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_SKULLS.value:
            await GetSkullsConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.ADD_WORD.value:
            await AddWordConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.SEND_GUESSES.value:
            await SendGuessesConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.START_GAME.value:
            await StartGameConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_RESULTS.value:
            await GetResultsConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.RECONNECT_PLAYER.value:
            await ReconnectPlayerConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_CONSTRAINTS.value:
            await GetConstraintsConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.SET_CONSTRAINT_AMOUNT.value:
            await SetConstraintAmountConsumer.treat_call(self, text_data_json)
        elif request_type == RequestTypeEnum.PICK_NEW_CHARACTER.value:
            await PickNewCharacterConsumer.treat_call(self, text_data_json)

    # Respond to a broadcast
    async def respond(self, event):
        res_type = event['res_type']
        if res_type == RequestTypeEnum.RESET.value:
            await ResetConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.REGISTER_PLAYER.value:
            await RegisterPlayerConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.GET_PLAYERS.value:
            await GetPlayersConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.REMOVE_PLAYER.value:
            await RemovePlayerConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.GET_SKULLS.value:
            await GetSkullsConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.ADD_WORD.value:
            await AddWordConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.SEND_GUESSES.value:
            await SendGuessesConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.START_GAME.value:
            await StartGameConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.GET_RESULTS.value:
            await GetResultsConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.RECONNECT_PLAYER.value:
            await ReconnectPlayerConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.GET_CONSTRAINTS.value:
            await GetConstraintsConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.SET_CONSTRAINT_AMOUNT.value:
            await SetConstraintAmountConsumer.respond(self, event)
        elif res_type == RequestTypeEnum.PICK_NEW_CHARACTER.value:
            await PickNewCharacterConsumer.respond(self, event)

    async def validate_call(self, text_data_json):
        request_type = text_data_json['type']
        if request_type == RequestTypeEnum.RESET.value:
            return await ResetConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.REGISTER_PLAYER.value:
            return await RegisterPlayerConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_PLAYERS.value:
            return await GetPlayersConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_SKULLS.value:
            return await GetSkullsConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.ADD_WORD.value:
            return await AddWordConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.SEND_GUESSES.value:
            return await SendGuessesConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.RECONNECT_PLAYER.value:
            return await ReconnectPlayerConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.GET_CONSTRAINTS.value:
            return await GetConstraintsConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.SET_CONSTRAINT_AMOUNT.value:
            return await SetConstraintAmountConsumer.valid_call(self, text_data_json)
        elif request_type == RequestTypeEnum.PICK_NEW_CHARACTER.value:
            return await PickNewCharacterConsumer.valid_call(self, text_data_json)
