# Generated by Django 3.0.4 on 2020-10-15 08:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='player',
            name='connected',
            field=models.BooleanField(default=True),
        ),
    ]
