from backend.models import Game, Skull, GameConstraint
from backend.serializers import GameSerializer, ResultSkullSerializer, GuessSkullSerializer, CharacterSerializer, \
    ConstraintSerializer


def get_game(game_id, serialized=False):
    game = Game.objects.filter(game_id=game_id)[0]
    if serialized:
        return GameSerializer(game).data
    else:
        return game


def get_skulls(game_id, serialized=False, results=False):
    skulls = Skull.objects.filter(game_id=game_id)
    if serialized:
        if results:
            return ResultSkullSerializer(skulls, many=True).data
        else:
            return GuessSkullSerializer(skulls, many=True).data
    else:
        return skulls


def get_characters(game_id):
    return CharacterSerializer(Skull.objects.filter(game_id=game_id), many=True).data


def get_constraints(game_id):
    return ConstraintSerializer(GameConstraint.objects.filter(game_id=game_id), many=True).data


def serialize_game(game):
    return GameSerializer(game).data


def get_character_names(game_id):
    skulls = get_skulls(game_id)
    return list(map(lambda skull: skull.character, skulls))
