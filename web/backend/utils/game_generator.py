﻿import random

from backend.models import Character, Skull, Game, GameConstraint, Constraint
from backend.utils.game_states import GameStateEnum
from backend.utils.player_getters import get_players
from backend.utils.player_utils import set_player_index


def generate_game(game_id, private):
    Game.objects.create(game_id=game_id,
                        state=GameStateEnum.LOBBY.value,
                        private=private)


def init_game(game_id):
    game = Game.objects.filter(game_id=game_id)[0]
    players = get_players(game_id)
    characters = select_characters()
    constraints = select_constraints(game.constraints)

    for i in range(len(players)):
        set_player_index(game_id, players[i].name, i)
        Skull.objects.create(game_id=game_id,
                             index=i,
                             character=characters[i],
                             played=True)

    for i in range(len(players), 8):
        Skull.objects.create(game_id=game_id,
                             index=i,
                             character=characters[i],
                             played=False)

    for i in range(len(constraints)):
        GameConstraint.objects.create(game_id=game_id,
                                      index=i,
                                      constraint=constraints[i])


def select_characters():
    characters = Character.objects.all()
    return list(map(lambda w: w.character, random.sample(list(characters), k=8)))


def select_constraints(amount):
    constraints = Constraint.objects.all()
    return list(map(lambda w: w.constraint, random.sample(list(constraints), k=amount)))
