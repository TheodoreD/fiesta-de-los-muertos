﻿from backend.models import Player


def add_player(game_id, player_name):
    Player.objects.create(game_id=game_id, name=player_name)


def remove_player(game_id, player_name):
    Player.objects.filter(game_id=game_id, name=player_name).delete()


def clean_players(game_id):
    Player.objects.filter(game_id=game_id, connected=False).delete()


def delete_players(game_id):
    Player.objects.filter(game_id=game_id).delete()