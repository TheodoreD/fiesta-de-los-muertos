from backend.models import Player
from backend.serializers import PlayerSerializer, ReconnectPlayerSerializer


def get_players(game_id, serialized=False):
    players = Player.objects.filter(game_id=game_id)
    if serialized:
        return PlayerSerializer(players, many=True).data
    return players


def get_player(game_id, player_name, serialized=False):
    player = Player.objects.filter(game_id=game_id, name=player_name)
    if serialized:
        return PlayerSerializer(player[0]).data
    return player


def get_reconnect_player_statuses(game_id):
    return ReconnectPlayerSerializer(get_players(game_id), many=True).data
