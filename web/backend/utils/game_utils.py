from backend.models import Skull, Guess, Character
from backend.utils.game_manager import get_state
from backend.utils.game_states import GameStateEnum
from backend.utils.player_utils import get_player_count


def state_finished(game_id):
    state = get_state(game_id)

    if state == GameStateEnum.FST_WORD:
        return all(map(lambda skull: len(skull.fst_word) > 0, Skull.objects.filter(game_id=game_id, played=True)))
    elif state == GameStateEnum.SEC_WORD:
        return all(map(lambda skull: len(skull.sec_word) > 0, Skull.objects.filter(game_id=game_id, played=True)))
    elif state == GameStateEnum.TRD_WORD:
        return all(map(lambda skull: len(skull.trd_word) > 0, Skull.objects.filter(game_id=game_id, played=True)))
    elif state == GameStateEnum.FTH_WORD:
        return all(map(lambda skull: len(skull.fth_word) > 0, Skull.objects.filter(game_id=game_id, played=True)))
    elif state == GameStateEnum.GUESSES:
        for i in range(get_player_count(game_id)):
            if len(Guess.objects.filter(game_id=game_id, player_index=i)) == 0:
                return False
        return True


def random_characters():
    return list(Character.objects.all().order_by('?')[:9])


def change_skull_character(game_id, skull_index, new_character):
    Skull.objects.filter(game_id=game_id, index=skull_index).update(character=new_character)
