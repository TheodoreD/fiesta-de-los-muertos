﻿from enum import Enum


class GameStateEnum(Enum):
    FST_WORD = 0
    SEC_WORD = 1
    TRD_WORD = 2
    FTH_WORD = 3
    GUESSES = 4
    FINISHED = 5
    LOBBY = 6
