from backend.models import Skull, Player, Guess
from backend.utils.game_manager import get_state
from backend.utils.game_states import GameStateEnum


def get_played_skull(game_id, player_index, game_state):
    player_amount = get_player_count(game_id)
    if game_state == GameStateEnum.FST_WORD:
        return player_index
    elif game_state == GameStateEnum.SEC_WORD:
        return (player_index + 1) % player_amount
    elif game_state == GameStateEnum.TRD_WORD:
        return (player_index + 2) % player_amount
    elif game_state == GameStateEnum.FTH_WORD:
        return (player_index + 3) % player_amount


def get_state_word(game_id, player_index, game_state):
    skull_index = get_played_skull(game_id, player_index, game_state)

    skull = Skull.objects.filter(game_id=game_id, index=skull_index)[0]

    if game_state == GameStateEnum.FST_WORD:
        return skull.character
    elif game_state == GameStateEnum.SEC_WORD:
        return skull.fst_word
    elif game_state == GameStateEnum.TRD_WORD:
        return skull.sec_word
    elif game_state == GameStateEnum.FTH_WORD:
        return skull.trd_word


def get_player_count(game_id):
    return len(Player.objects.filter(game_id=game_id))


def get_player_index(game_id, player_name):
    return Player.objects.filter(game_id=game_id, name=player_name)[0].index


def set_player_index(game_id, player_name, index):
    Player.objects.filter(game_id=game_id, name=player_name).update(index=index)


def has_guesses(game_id, player_index):
    return len(Guess.objects.filter(game_id=game_id, player_index=player_index)) > 0


def set_player_connected(game_id, player_name, value):
    Player.objects.filter(game_id=game_id, name=player_name).update(connected=value)


def disconnected_player_exists(game_id, player_name):
    player = Player.objects.filter(game_id=game_id, name=player_name, connected=False)
    return len(player) > 0


def player_exists(game_id, player_name):
    player = Player.objects.filter(game_id=game_id, name=player_name)
    return len(player) > 0


def has_played(game_id, player_index):
    state = get_state(game_id)
    if state == GameStateEnum.GUESSES:
        return has_guesses(game_id, player_index)

    skull_index = get_played_skull(game_id, player_index, state)
    skull = Skull.objects.filter(game_id=game_id, index=skull_index)[0]
    if state == GameStateEnum.FST_WORD:
        return len(skull.fst_word) > 0
    elif state == GameStateEnum.SEC_WORD:
        return len(skull.sec_word) > 0
    elif state == GameStateEnum.TRD_WORD:
        return len(skull.trd_word) > 0
    elif state == GameStateEnum.FTH_WORD:
        return len(skull.fth_word) > 0

    return True
