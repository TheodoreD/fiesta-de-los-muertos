from django.urls import path
from . import views

urlpatterns = [
    path('api/createGame', views.CreateGame.as_view()),
    path('api/publicGames', views.PublicGameList.as_view()),
]