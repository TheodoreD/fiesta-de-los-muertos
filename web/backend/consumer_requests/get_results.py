﻿from channels.db import database_sync_to_async

import json

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumer_requests.request_types import RequestTypeEnum
from backend.utils.game_getters import get_game, get_skulls, get_characters


class GetResultsConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        game = await database_sync_to_async(get_game)(consumer.game_id, True)
        result_skulls = await database_sync_to_async(get_skulls)(consumer.game_id, True, True)
        characters = await database_sync_to_async(get_characters)(consumer.game_id)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.GET_RESULTS.value,
            'game': game,
            'result_skulls': result_skulls,
            'characters': characters,
        }))

    @staticmethod
    async def respond(consumer, event):
        pass

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return True
