﻿import json

from channels.db import database_sync_to_async

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumer_requests.request_types import RequestTypeEnum
from backend.utils.game_getters import get_skulls, get_characters
from backend.utils.game_manager import get_state
from backend.utils.game_states import GameStateEnum
from backend.utils.player_getters import get_player, get_reconnect_player_statuses
from backend.utils.player_utils import get_state_word, get_player_index, set_player_connected, \
    disconnected_player_exists


class ReconnectPlayerConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        name = text_data_json['name']

        if not await database_sync_to_async(disconnected_player_exists)(consumer.game_id, name):
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.RECONNECT_PLAYER.value,
                'error': 1
            }))
            return

        await database_sync_to_async(set_player_connected)(consumer.game_id, name, True)

        consumer.player_name = name
        consumer.index = await database_sync_to_async(get_player_index)(consumer.game_id, consumer.player_name)

        # Send message to room group
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.RECONNECT_PLAYER.value,
                'player_name': consumer.player_name,
            }
        )

    @staticmethod
    async def respond(consumer, event):
        player_name = event['player_name']

        player = await database_sync_to_async(get_player)(consumer.game_id, player_name, True)

        if player_name != consumer.player_name:
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.RECONNECT_PLAYER.value,
                'error': 0,
                'self': False,
                'player': player,
            }))
        else:
            state = await database_sync_to_async(get_state)(consumer.game_id)
            if state == GameStateEnum.GUESSES:
                await consumer.send(text_data=json.dumps({
                    'type': RequestTypeEnum.RECONNECT_PLAYER.value,
                    'error': 0,
                    'self': True,
                    'player': player,
                    'skulls': await database_sync_to_async(get_skulls)(consumer.game_id, True, False),
                    'player_statuses': await database_sync_to_async(get_reconnect_player_statuses)(consumer.game_id)
                }))
            elif state == GameStateEnum.FINISHED:
                result_skulls = await database_sync_to_async(get_skulls)(consumer.game_id, True, True)
                characters = await database_sync_to_async(get_characters)(consumer.game_id)
                await consumer.send(text_data=json.dumps({
                    'type': RequestTypeEnum.RECONNECT_PLAYER.value,
                    'error': 0,
                    'self': True,
                    'player': player,
                    'result_skulls': result_skulls,
                    'characters': characters,
                }))
            else:
                await consumer.send(text_data=json.dumps({
                    'type': RequestTypeEnum.RECONNECT_PLAYER.value,
                    'error': 0,
                    'self': True,
                    'player': player,
                    'word': await database_sync_to_async(get_state_word)(consumer.game_id, consumer.index, state),
                    'player_statuses': await database_sync_to_async(get_reconnect_player_statuses)(consumer.game_id)
                }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return 'name' in text_data_json and 0 < len(text_data_json['name']) <= 16
