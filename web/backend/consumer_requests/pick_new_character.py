﻿from channels.db import database_sync_to_async

import json

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumer_requests.request_types import RequestTypeEnum
from backend.utils.game_getters import get_skulls, get_character_names
from backend.utils.game_manager import get_state
from backend.utils.game_states import GameStateEnum
from backend.utils.game_utils import random_characters, change_skull_character


class PickNewCharacterConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        skull_characters = await database_sync_to_async(get_character_names)(consumer.game_id)
        new_characters = await database_sync_to_async(random_characters)()
        new_character = ''

        for character in new_characters:
            if character.character not in skull_characters:
                new_character = character.character
                break

        await database_sync_to_async(change_skull_character)(consumer.game_id, consumer.index, new_character)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.PICK_NEW_CHARACTER.value,
            'new_character': new_character,
        }))

    @staticmethod
    async def respond(consumer, event):
        pass

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return await database_sync_to_async(get_state)(consumer.game_id) == GameStateEnum.FST_WORD
