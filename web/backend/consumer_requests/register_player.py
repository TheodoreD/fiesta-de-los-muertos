﻿import json

from channels.db import database_sync_to_async

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumer_requests.request_types import RequestTypeEnum
from backend.utils import game_manager
from backend.utils.game_getters import get_game
from backend.utils.player_manager import add_player
from backend.utils.player_getters import get_player
from backend.utils.player_utils import player_exists


class RegisterPlayerConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        name = text_data_json['name']

        if await database_sync_to_async(player_exists)(consumer.game_id, name):
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.REGISTER_PLAYER.value,
                'error': 1
            }))
            return

        consumer.player_name = name

        await database_sync_to_async(add_player)(consumer.game_id, consumer.player_name)

        await database_sync_to_async(game_manager.join)(consumer.game_id)

        # Send message to room group
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.REGISTER_PLAYER.value,
                'player_name': consumer.player_name,
            }
        )

    @staticmethod
    async def respond(consumer, event):
        player_name = event['player_name']

        game = await database_sync_to_async(get_game)(consumer.game_id, True)
        player = await database_sync_to_async(get_player)(consumer.game_id, player_name, True)

        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.REGISTER_PLAYER.value,
            'error': 0,
            'self': player_name == consumer.player_name,
            'player': player,
            'game': game
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return 'name' in text_data_json and 0 < len(text_data_json['name']) <= 16
