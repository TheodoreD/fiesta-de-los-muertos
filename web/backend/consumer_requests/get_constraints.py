﻿from channels.db import database_sync_to_async

import json

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumer_requests.request_types import RequestTypeEnum
from backend.utils.game_getters import get_constraints


class GetConstraintsConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        # Send message to WebSocket
        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.GET_CONSTRAINTS.value,
            'constraints': await database_sync_to_async(get_constraints)(consumer.game_id),
        }))

    @staticmethod
    async def respond(consumer, event):
        pass

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return True
