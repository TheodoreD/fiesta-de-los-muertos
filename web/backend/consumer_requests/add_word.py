﻿import json
import re

from channels.db import database_sync_to_async

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumer_requests.request_types import RequestTypeEnum
from backend.utils.game_manager import get_state, play_word, next_state
from backend.utils.game_utils import state_finished
from backend.utils.game_getters import get_game, serialize_game
from backend.utils.game_states import GameStateEnum
from backend.utils.player_utils import get_played_skull, get_state_word


class AddWordConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        game_id = consumer.game_id
        player_name = consumer.player_name
        player_index = consumer.index
        game_state = await database_sync_to_async(get_state)(game_id)

        played_skull = await database_sync_to_async(get_played_skull)(game_id, player_index, game_state)
        await database_sync_to_async(play_word)(game_id, game_state, played_skull, text_data_json['word'])

        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.ADD_WORD.value,
            'error': 0,
            'ack': True
        }))

        new_state = await database_sync_to_async(state_finished)(game_id)
        if new_state:
            await database_sync_to_async(next_state)(game_id)

        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.ADD_WORD.value,
                'player': player_name,
                'new_state': new_state,
            }
        )

    @staticmethod
    async def respond(consumer, event):
        game = await database_sync_to_async(get_game)(consumer.game_id)
        if event['new_state'] and game.state != GameStateEnum.GUESSES.value:
            new_word = await database_sync_to_async(get_state_word)(consumer.game_id, consumer.index, GameStateEnum(game.state))
        else:
            new_word = ""

        await consumer.send(text_data=json.dumps({
            'type': RequestTypeEnum.ADD_WORD.value,
            'error': 0,
            'game': await database_sync_to_async(serialize_game)(game),
            'player': event['player'],
            'word': new_word,
        }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return 'word' in text_data_json and re.match(r"^[^ \t\n\r\f\v_.]+$", text_data_json['word'])
