﻿import json

from channels.db import database_sync_to_async

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumer_requests.request_types import RequestTypeEnum
from backend.utils.game_generator import init_game
from backend.utils.game_manager import game_started, set_game_started, set_state
from backend.utils.game_getters import get_game, get_constraints
from backend.utils.game_states import GameStateEnum
from backend.utils.player_getters import get_players
from backend.utils.player_utils import get_state_word, get_player_index


class StartGameConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        game_id = consumer.game_id

        # Send first message to avoid other players clicking button
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.START_GAME.value,
                'creating': True
            }
        )

        if await database_sync_to_async(game_started)(game_id):
            return

        await database_sync_to_async(set_game_started)(game_id, True)
        await database_sync_to_async(init_game)(game_id)
        await database_sync_to_async(set_state)(game_id, GameStateEnum.FST_WORD)

        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.START_GAME.value,
                'creating': False
            }
        )

    @staticmethod
    async def respond(consumer, event):
        if event['creating']:
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.START_GAME.value,
                'error': 0,
                'creating': True,
            }))
        else:
            consumer.index = await database_sync_to_async(get_player_index)(consumer.game_id, consumer.player_name)
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.START_GAME.value,
                'error': 0,
                'creating': False,
                'game': await database_sync_to_async(get_game)(consumer.game_id, True),
                'word': await database_sync_to_async(get_state_word)(consumer.game_id, consumer.index, GameStateEnum.FST_WORD),
                'players': await database_sync_to_async(get_players)(consumer.game_id, True),
                'constraints': await database_sync_to_async(get_constraints)(consumer.game_id),
            }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return True
