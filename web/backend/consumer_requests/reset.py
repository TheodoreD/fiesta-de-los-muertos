﻿from asgiref.sync import sync_to_async
from channels.db import database_sync_to_async

import json

from backend.consumer_requests.consumer_request import ConsumerRequest
from backend.consumers import RequestTypeEnum
from backend.utils.game_generator import generate_game
from backend.utils.game_manager import delete_game, is_private
from backend.utils.game_getters import get_game
from backend.utils.player_manager import clean_players
from backend.utils.player_getters import get_players


class ResetConsumer(ConsumerRequest):
    @staticmethod
    async def treat_call(consumer, text_data_json):
        # Send message to hide Reset button while resetting
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.RESET.value,
                'resetting': True
            }
        )

        private = await database_sync_to_async(is_private)(consumer.game_id)

        await database_sync_to_async(delete_game)(consumer.game_id)
        await database_sync_to_async(generate_game)(consumer.game_id, private)
        await database_sync_to_async(clean_players)(consumer.game_id)

        # Send message to room group
        await consumer.channel_layer.group_send(
            consumer.game_group_name,
            {
                'type': 'respond',
                'res_type': RequestTypeEnum.RESET.value,
                'resetting': False
            }
        )

    @staticmethod
    async def respond(consumer, event):
        if event['resetting']:
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.RESET.value,
                'resetting': True
            }))
        else:
            game = await database_sync_to_async(get_game)(consumer.game_id, True)

            # Send message to WebSocket
            await consumer.send(text_data=json.dumps({
                'type': RequestTypeEnum.RESET.value,
                'game': game,
                'resetting': False,
                'players': await database_sync_to_async(get_players)(consumer.game_id, True)
            }))

    @staticmethod
    async def valid_call(consumer, text_data_json):
        return True
