from rest_framework import serializers
from .models import Skull, Player, Game, Guess, GameConstraint
from .utils.player_utils import has_played


class ResultSkullSerializer(serializers.ModelSerializer):
    player_guesses = serializers.SerializerMethodField('get_player_guesses')

    def get_player_guesses(self, skull):
        guesses = Guess.objects.filter(game_id=skull.game_id, skull_index=skull.index)
        return GuessSerializer(guesses, many=True).data

    class Meta:
        model = Skull
        fields = ('game_id', 'index', 'played', 'character', 'fst_word', 'sec_word', 'trd_word', 'fth_word', 'player_guesses')


class GuessSkullSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skull
        fields = ('game_id', 'index', 'played', 'character', 'fth_word')


class CharacterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Skull
        fields = ('index', 'character')


class ConstraintSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameConstraint
        fields = ('index', 'constraint')


class GuessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guess
        fields = ('game_id', 'player_index', 'skull_index', 'character_index')


class PlayerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Player
        fields = ('game_id', 'name', 'index', 'connected')


class ReconnectPlayerSerializer(serializers.ModelSerializer):
    has_finished = serializers.SerializerMethodField('get_has_finished')

    def get_has_finished(self, player):
        return has_played(player.game_id, player.index)

    class Meta:
        model = Player
        fields = ('game_id', 'name', 'index', 'has_finished')


class GameSerializer(serializers.ModelSerializer):
    players_amount = serializers.SerializerMethodField('get_players_amount')

    def get_players_amount(self, game):
        return len(Player.objects.filter(game_id=game.game_id))

    class Meta:
        model = Game
        fields = ('game_id', 'state', 'started', 'constraints', 'players_amount')
