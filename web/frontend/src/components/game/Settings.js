﻿import React, {Component} from "react";
import IconButton from "@material-ui/core/IconButton";
import SettingsIcon from '@material-ui/icons/Settings'
import './Settings.css'

class Settings extends Component {
    constructor(props) {
        super(props);
        
        this.state = {opened: false};
    }

    render() {
        return (
            <div>
                <IconButton
                    aria-label="settings"
                    style={{color: "white"}}
                    onClick={() => this.setState({opened: !this.state.opened})}>
                    <SettingsIcon/>
                </IconButton>
                <div className="SettingsPanel" style={{display: this.state.opened ? "block" : "none"}}>
                    
                </div>
            </div>
        );
    }
}

export default Settings;