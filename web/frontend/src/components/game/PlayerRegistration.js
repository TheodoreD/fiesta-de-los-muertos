﻿import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import {Button, Input} from "@material-ui/core";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import "./PlayerRegistration.css"
import Checkbox from "@material-ui/core/Checkbox";

class PlayerRegistration extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            registering: false,
            name: '',
            err: false,
            sendPlayer: props.sendPlayer,
            sendReconnect: props.sendReconnect,
            spectator: false,
            selectedReconnect: '',
        };
        
        this.register = this.register.bind(this);
        this.reconnect = this.reconnect.bind(this);
        this.drawBaseRegistration = this.drawBaseRegistration.bind(this);
        this.drawPlayerReconnection = this.drawPlayerReconnection.bind(this);
    }
    
    register() {
        this.setState({registering: true, err: false});
        if (this.checkPlayer(this.state.name)) {
            this.state.sendPlayer(this.state.name);
        }
        else {
            this.setState({registering: false, err: true})
        }
    }
    
    reconnect() {
        this.setState({registering: true, err: false});
        if (this.props.players.find(player => player.name === this.state.selectedReconnect && !player.connected)) {
            this.state.sendReconnect(this.state.selectedReconnect);
        }
        else {
            this.setState({registering: false, err: true})
        }
    }
    
    checkPlayer(name) {
        return name && name.length > 0 && name.length <= 16;
    }
    
    componentDidUpdate(prevProps) {
        if (this.props.try !== prevProps.try) {
            this.setState({registering: false, err: true})
        }
    }
    
    drawBaseRegistration() {
        return (
            <Grid style={{width: "50%"}} container justify="center" spacing={1} className="PlayerRegistrationForm">
                <Grid item xs={10}>
                    {
                        this.state.err &&
                        <span style={{color: "red"}}>
                            Erreur : ce joueur existe déja ou les informations rentrées sont invalides.
                            Ton nom doit avoir entre 1 et 16 caractères
                        </span>
                    }
                </Grid>
                <Grid item xs={7}>
                    <span>Quel est ton nom ?</span>
                </Grid>
                <Grid item xs={7}>
                    <Input fullWidth onChange={(event) => this.setState({name: event.target.value})}/>
                </Grid>
                {/*<Grid item xs={7}>*/}
                {/*    <FormControlLabel*/}
                {/*        control={*/}
                {/*            <Checkbox*/}
                {/*                checked={this.state.spectator}*/}
                {/*                onChange={event => this.setState({spectator: event.target.checked})}*/}
                {/*                name="spectator"*/}
                {/*                color="primary"*/}
                {/*            />*/}
                {/*        }*/}
                {/*        label="Mode spectateur"*/}
                {/*    />*/}
                {/*</Grid>*/}
                <Grid item xs={7}>
                    <Button onClick={this.register} variant="contained" color="primary">
                        M'enregistrer
                    </Button>
                </Grid>
            </Grid>
        );
    }

    drawPlayerReconnection() {
        return (
            <Grid style={{width: "50%"}} container justify="center" spacing={1} className="PlayerRegistrationForm">
                <Grid item xs={10}>
                    {
                        this.state.err &&
                        <span style={{color: "red"}}>
                            Erreur
                        </span>
                    }
                </Grid>
                <Grid item xs={7}>
                    <span>Reconnexion : qui es-tu ?</span>
                </Grid>
                <Grid item xs={7}>
                    <RadioGroup aria-label="playerReconnect" name="playerReconnect1"
                                onChange={(event) => this.setState({selectedReconnect: event.target.value})}>
                        {
                            this.props.players.filter(player => !player.connected).map((player, i) =>
                                <FormControlLabel key={i} value={player.name} control={<Radio color="primary"/>} label={player.name}/>
                            )
                        }
                    </RadioGroup>
                </Grid>
                <Grid item xs={7}>
                    <Button onClick={this.reconnect} variant="contained" color="primary">
                        Reconnexion
                    </Button>
                </Grid>
            </Grid>
        );
    }

    render() {
        if (this.state.registering) {
            return (
                <Grid style={{width: "50%"}} container justify="center" spacing={1} className="PlayerRegistrationForm">
                    <Grid item xs={7} style={{height: "50px"}}/>
                    <Grid item xs={7}>
                        <span>Enregistrement...</span>
                    </Grid>
                    <Grid item xs={7} style={{height: "50px"}}/>
                </Grid>
            );
        }
        
        if (this.props.started) {
            return this.drawPlayerReconnection();
        }
        
        return this.drawBaseRegistration();
    }
}

export default PlayerRegistration;