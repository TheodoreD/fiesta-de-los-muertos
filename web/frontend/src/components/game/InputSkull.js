import React from "react";
import './InputSkull.css'
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

class InputSkull extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sendWord: props.sendWord,
            error: false,
            word: '',
        };
        
        this.sendWord = this.sendWord.bind(this);
        this.validWord = this.validWord.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (this.props.prevWord !== prevProps.prevWord) {
            this.setState({word: ''})
        }
    }
    
    sendWord() {
        this.setState({error: false});
        if (this.validWord(this.state.word)) {
            this.state.sendWord(this.state.word);
        }
        else {
            this.setState({error: true});
        }
    }

    validWord(word) {
        return word.length > 0 && word.length <= 32 && word.match(/^[^ \t\n\r\f\v_.]+$/);
    }

    render() {
        return (
            <Grid container item xs={5} justify="center" spacing={1} className="skull">
                <Grid item xs={12}>
                    <span className={"prevWord"}>
                        {this.props.prevWord}
                    </span>
                </Grid>
                <Grid item xs={7}>
                    <TextField label={"Mot associé"} value={this.state.word} fullWidth onChange={(event) => this.setState({word: event.target.value})} disabled={this.props.ack}/>
                </Grid>
                <Grid item xs={7}>
                    {
                        !this.props.ack &&
                        <Button onClick={this.sendWord} variant="contained" color="primary" disabled={this.props.ack}>
                            Envoyer le mot
                        </Button>
                    }
                    {
                        this.props.ack &&
                        <span>
                            Mot enregistré !
                        </span>
                    }
                </Grid>
                {
                    this.state.error &&
                    <Grid item xs={12}>
                        <span style={{color: "red"}}>
                            Votre mot est invalide. Il doit avoir entre 1 et 32 caractères et ne faire qu'un seul mot ('-' acceptés pour les mots composés).
                        </span>
                    </Grid>
                }
                <Grid item xs={12}>
                    <span>
                        Tour {this.props.turn}/4
                    </span>
                </Grid>
            </Grid>
        )
    }
}

export default InputSkull;