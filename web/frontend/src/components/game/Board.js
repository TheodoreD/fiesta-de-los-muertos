﻿import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import "./Board.css"
import PlayerList from "./PlayerList";
import {GameStateEnum} from "../../utils/enums.js";
import InputSkull from "./InputSkull";
import GuessSkull from "./GuessSkull";
import Button from "@material-ui/core/Button";
import ResultSkull from "./ResultSkull";

class Board extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            guesses: {},
            sendGuesses: props.sendGuesses,
            missingGuesses: true,
            askNewCharacter: props.askNewCharacter,
        };
        
        this.turnNumber = this.turnNumber.bind(this);
        this.setGuess = this.setGuess.bind(this);
        this.sendGuesses = this.sendGuesses.bind(this);
        this.drawPause = this.drawPause.bind(this);
        this.drawWordState = this.drawWordState.bind(this);
        this.drawGuessState = this.drawGuessState.bind(this);
        this.drawResultsState = this.drawResultsState.bind(this);
    }
    
    turnNumber() {
        switch (this.props.game.state) {
            case GameStateEnum.FST_WORD:
                return 1;
            case GameStateEnum.SEC_WORD:
                return 2;
            case GameStateEnum.TRD_WORD:
                return 3;
            case GameStateEnum.FTH_WORD:
                return 4;
        }
    }
    
    setGuess(skullIndex, characterIndex) {
        let guesses = this.state.guesses;
        guesses[skullIndex] = characterIndex;
        this.setState({
            guesses: guesses,
            missingGuesses: Object.keys(guesses).length < this.props.guessWords.length,
        })
    }
    
    sendGuesses() {
        if (!this.state.missingGuesses) {
            this.state.sendGuesses(this.state.guesses);
        }
    }
    
    drawPause() {
        return (
            this.props.paused &&
            <Grid container item xs={3} justify="center" spacing={1} className="pauseDisplay">
                <Grid item xs={12}>
                    <h2>
                        Partie en pause
                    </h2>
                </Grid>
                <Grid item xs={8}>
                    <span>
                        En attente de reconnexion de :
                    </span>
                    <ul>
                        {
                            this.props.players.filter(player => !player.connected).map((player, i) =>
                                <li className={"playerWaited"}>{player.name}</li>
                            )
                        }
                    </ul>
                </Grid>
            </Grid>
        )
    }
    
    drawWordState() {
        return (
            <Grid container justify="center" spacing={3}>
                {this.drawPause()}
                <Grid item xs={12}/>
                <InputSkull prevWord={this.props.prevWord} sendWord={this.props.sendWord} ack={this.props.ack} turn={this.turnNumber()}/>
                <Grid item xs={12}/>
                {
                    this.turnNumber() === 1 &&
                    !this.props.ack &&
                    <Grid item xs={5}>
                        <Button variant="contained" color="primary" onClick={this.state.askNewCharacter} >Demander un autre personnage</Button>
                    </Grid>
                }
                <Grid item xs={12}/>
                {
                    this.props.constraints.length >= this.turnNumber() &&
                    <Grid item xs={3}>
                        <div className={"constraint-card"}>
                            <span className={"constraint"}>{this.props.constraints.find(c => c.index === this.turnNumber()-1).constraint}</span>
                        </div>
                    </Grid>
                }
                <Grid item xs={12}/>
                <Grid item xs={8}>
                    <PlayerList players={this.props.players}/>
                </Grid>
            </Grid>
        );
    }

    drawGuessState() {
        if (this.props.characters.length === 0 || this.props.guessWords.length === 0) {
            return (
                <Grid style={{width: "50%"}} container justify="center" spacing={1} className="ResettingPanel">
                    <Grid item xs={7} style={{height: "50px"}}/>
                    <Grid item xs={7}>
                        <span>Chargement des données...</span>
                    </Grid>
                    <Grid item xs={7} style={{height: "50px"}}/>
                </Grid>
            )
        }
        
        return (
            <Grid container justify="center" spacing={4}>
                {this.drawPause()}
                <Grid item xs={12}/>
                <Grid container item xs={10} justify="center" spacing={2}>
                    {
                        this.props.characters.map((character, i) =>
                            <Grid key={i} item xs={3}>
                                <div className={"character-card"}>
                                    <span className={"character-name"}>{character.name}</span>
                                </div>
                            </Grid>
                        )
                    }
                </Grid>
                <Grid container item xs={12} justify="center" spacing={2}>
                    {
                        this.props.guessWords.map((guessWord, i) =>
                            <Grid key={i} container item xs={3} justify="center" spacing={1}>
                                <GuessSkull guessWord={guessWord.word} setGuess={(characterIndex) => {this.setGuess(guessWord.index, characterIndex)}} characters={this.props.characters} ack={this.props.ack}/>
                            </Grid>
                        )
                    }
                </Grid>
                <Grid item xs={5}>
                    <Button variant="contained" color="primary" onClick={this.sendGuesses} disabled={this.state.missingGuesses || this.props.ack}>Envoyer les suggestions</Button>
                </Grid>
                <Grid container item xs={12} justify="center" spacing={2}>
                    {
                        this.props.constraints.sort((c1, c2) => c1.index - c2.index).map((constraint, i) =>
                            <Grid key={i} item xs={3}>
                                <div className={"constraint-card"}>
                                    <span className={"constraint"}>{constraint.constraint}</span>
                                </div>
                            </Grid>
                        )
                    }
                </Grid>
                <Grid item xs={8}>
                    <PlayerList players={this.props.players}/>
                </Grid>
            </Grid>
        );
    }

    drawResultsState() {
        if (this.props.characters.length === 0 || this.props.results.length === 0) {
            return (
                <Grid style={{width: "50%"}} container justify="center" spacing={1} className="ResettingPanel">
                    <Grid item xs={7} style={{height: "50px"}}/>
                    <Grid item xs={7}>
                        <span>Chargement des résultats...</span>
                    </Grid>
                    <Grid item xs={7} style={{height: "50px"}}/>
                </Grid>
            )
        }

        return (
            <Grid container justify="center" spacing={4}>
                <Grid container item xs={12} justify="center" spacing={2}>
                    {
                        this.props.results.filter(result => result.played).map((result, i) =>
                            <Grid key={i} container item xs={3} justify="center" spacing={1}>
                                <ResultSkull result={result} characters={this.props.characters} players={this.props.players}/>
                            </Grid>
                        )
                    }
                </Grid>
                <Grid container item xs={12} justify="center" spacing={2}>
                    {
                        this.props.constraints.sort((c1, c2) => c1.index - c2.index).map((constraint, i) =>
                            <Grid key={i} item xs={3}>
                                <div className={"constraint-card"}>
                                    <span className={"constraint"}>{constraint.constraint}</span>
                                </div>
                            </Grid>
                        )
                    }
                </Grid>
            </Grid>
        );
    }

    render() {
        switch (this.props.game.state) {
            case GameStateEnum.FST_WORD:
                return this.drawWordState();
            case GameStateEnum.SEC_WORD:
                return this.drawWordState();
            case GameStateEnum.TRD_WORD:
                return this.drawWordState();
            case GameStateEnum.FTH_WORD:
                return this.drawWordState();
            case GameStateEnum.GUESSES:
                return this.drawGuessState();
            case GameStateEnum.FINISHED:
                return this.drawResultsState();
        }
    }
}

export default Board;