import React, {Component} from 'react';
import './Game.css';
import ReconnectingWebSocket from 'reconnecting-websocket'
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import PlayerRegistration from "./PlayerRegistration";
import {GameStateEnum, RequestTypeEnum} from "../../utils/enums.js";
import Settings from "./Settings";
import Lobby from "./Lobby";
import PlayerList from "./PlayerList";
import Board from "./Board";
import {shuffle} from "../../utils/shuffle";

class Game extends Component {
    constructor(props) {
        super(props);
        
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const gameId = urlParams.get("game_id");

        this.editColorBlind = this.editColorBlind.bind(this);
        this.playNotification = this.playNotification.bind(this);
        this.onMessage = this.onMessage.bind(this);
        this.treatMessage = this.treatMessage.bind(this);
        this.getPlayers = this.getPlayers.bind(this);
        this.setPlayers = this.setPlayers.bind(this);
        this.removePlayer = this.removePlayer.bind(this);
        this.addPlayer = this.addPlayer.bind(this);
        this.reconnectPlayer = this.reconnectPlayer.bind(this);
        this.sendPlayer = this.sendPlayer.bind(this);
        this.sendReconnect = this.sendReconnect.bind(this);
        this.sendWord = this.sendWord.bind(this);
        this.sendGuesses = this.sendGuesses.bind(this);
        this.sendReset = this.sendReset.bind(this);
        this.getSkulls = this.getSkulls.bind(this);
        this.getResults = this.getResults.bind(this);
        this.getConstraints = this.getConstraints.bind(this);
        this.startGame = this.startGame.bind(this);
        this.sendConstraintAmount = this.sendConstraintAmount.bind(this);
        this.canStart = this.canStart.bind(this);
        this.askNewCharacter = this.askNewCharacter.bind(this);
        
        let ws_scheme = window.location.protocol === "https:" ? "wss" : "ws";
        let webSocket = new ReconnectingWebSocket(ws_scheme + '://' + window.location.host + "/ws/game/" + gameId + "/");
        
        webSocket.onmessage = this.onMessage;
        
        this.state = {
            players: [],
            playerName: '',
            registerTry: 0,
            registered: false,
            started: false,
            loaded: false,
            gameId: gameId,
            webSocket: webSocket,
            game: null,
            resetting: false,
            colorBlind: false,
            prevWord: '',
            ack: false,
            characters: [],
            guessWords: [],
            results: [],
            paused: false,
            constraints: [],
            constraintAmount: 0,
        }
    }

    componentDidMount() {
        this.getPlayers();
        this.getConstraints();
    }

    editColorBlind(value) {
        this.setState({colorBlind: value});
    }
    
    playNotification(playerAction) {
        let notification = playerAction ?
                document.getElementById("notification")
            :
                document.getElementById("notification-low");
        // notification.play();
        // TODO : configure
    }
    
    onMessage(message) {
        this.treatMessage(JSON.parse(message.data));
    }

    treatMessage(data) {
        let changedState = false;
        if (data.game) {
            changedState = this.state.game !== null && this.state.game.state !== data.game.state;
            this.setState({game: data.game, started: data.game.started, constraintAmount: data.game.constraints});
        }

        switch (data.type) {
            case RequestTypeEnum.RESET:
                if (data.error !== 1) {
                    if (data.resetting) {
                        this.setState({resetting: true, started: false});
                    }
                    else {
                        this.playNotification(true)
                        this.setState({
                            resetting: false,
                            prevWord: '',
                            ack: false,
                            characters: [],
                            guessWords: [],
                            results: [],
                            constraints: [],
                            paused: false,
                        });
                        this.setPlayers(data);
                    }
                }
                break;
            case RequestTypeEnum.REGISTER_PLAYER:
                this.playNotification(false)
                this.addPlayer(data);
                break;
            case RequestTypeEnum.START_GAME:
                this.setState({
                    started: true,
                    resetting: data.creating,
                    prevWord: data.word ?? '',
                    players: data.players ?? this.state.players,
                    constraints: data.constraints,
                })
                break;
            case RequestTypeEnum.GET_PLAYERS:
                this.setPlayers(data);
                break;
            case RequestTypeEnum.REMOVE_PLAYER:
                this.playNotification(false)
                this.removePlayer(data);
                break;
            case RequestTypeEnum.RECONNECT_PLAYER:
                this.playNotification(false)
                this.reconnectPlayer(data);
                break;
            case RequestTypeEnum.ADD_WORD:
                if (data.ack) {
                    this.setState({ack: true})
                } else if (!changedState) {
                    this.setState({
                        players: this.state.players.map(player => {
                            if (player.name === data.player) {
                                player.hasFinished = true;
                            }
                            return player;
                        })
                    })
                } else {
                    this.setState({
                        players: this.state.players.map(player => {
                            player.hasFinished = false;
                            return player;
                        }),
                        ack: false,
                        prevWord: data.word
                    })
                    if (data.game.state === GameStateEnum.GUESSES) {
                        this.getSkulls();
                    }
                }
                break;
            case RequestTypeEnum.GET_SKULLS:
                let characters = [];
                let guessWords = [];
                
                data.skulls.map(skull => {
                    characters.push({name: skull.character, index: skull.index});
                    if (skull.played) {
                        guessWords.push({word: skull.fth_word, index: skull.index});
                    }
                });
                shuffle(characters);
                shuffle(guessWords);
                
                this.setState({
                    characters: characters,
                    guessWords: guessWords,
                });
                break;
            case RequestTypeEnum.SEND_GUESSES:
                if (data.ack) {
                    this.setState({ack: true})
                } else if (!changedState) {
                    this.setState({
                        players: this.state.players.map(player => {
                            if (player.name === data.player) {
                                player.hasFinished = true;
                            }
                            return player;
                        })
                    })
                } else {
                    this.setState({
                        players: this.state.players.map(player => {
                            player.hasFinished = false;
                            return player;
                        }),
                        ack: false,
                    })
                    this.getResults();
                }
                break;
            case RequestTypeEnum.GET_RESULTS:
                this.setState({
                    characters: data.characters,
                    results: data.result_skulls,
                });
                break;
            case RequestTypeEnum.GET_CONSTRAINTS:
                this.setState({constraints: data.constraints})
                break;
            case RequestTypeEnum.SET_CONSTRAINT_AMOUNT:
                this.setState({constraintAmount: data.amount,})
                break;
            case RequestTypeEnum.PICK_NEW_CHARACTER:
                this.setState({prevWord: data.new_character,})
                break;
            default:
                break;
        }
    }
    
    getPlayers() {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.GET_PLAYERS,
            game_id: this.state.gameId
        }));
    }
    
    setPlayers(data) {
        if (data.error !== 1) {
            this.setState({
                players: data.players
            });
        }
    }

    removePlayer(data) {
        if (this.state.game && this.state.game.state === GameStateEnum.LOBBY) {
            this.setState({
                players: this.state.players.filter(p => p.name !== data.name),
            });
        }
        else {
            this.setState({
                players: this.state.players.map(player => {
                    if (player.name === data.name) {
                        player.connected = false;
                    }
                    return player;
                }),
                paused: true,
            });
        }
    }

    addPlayer(data) {
        if (data.error === 1) {
            this.setState({
                playerName: '',
                registerTry: this.state.registerTry + 1
            });
            return;
        }
        
        if (data.self) {
            this.setState({
                playerName: data.player.name,
                registered: true});
        }
        
        let players = this.state.players;
        players.push(data.player);
        this.setState({players: players});
    }

    reconnectPlayer(data) {
        if (data.error === 1) {
            this.setState({
                playerName: '',
                registerTry: this.state.registerTry + 1
            });
            return;
        }

        if (data.self) {
            this.setState({
                playerName: data.player.name,
                registered: true,
                ack: data.player_statuses ? data.player_statuses.find(player => player.name === data.player.name).has_finished : true
            });
            
            if (data.word) {
                // State is a word state
                this.setState({
                    prevWord: data.word,
                })
            } else if (data.skulls) {
                // State is guessing state
                this.treatMessage({
                    type: RequestTypeEnum.GET_SKULLS,
                    skulls: data.skulls,
                });
            } else if (data.result_skulls) {
                // State is results state
                this.treatMessage({
                    type: RequestTypeEnum.GET_RESULTS,
                    result_skulls: data.result_skulls,
                    characters: data.characters,
                });
            }
        }

        let players = this.state.players.map(player => {
            if (player.name === data.player.name) {
                player.connected = true;
            }
            if (data.player_statuses) {
                let foundInStatuses = data.player_statuses.find(playerStat => playerStat.name === player.name);
                if (foundInStatuses) {
                    player.hasFinished = foundInStatuses.has_finished;
                }
            }
            return player;
        });
        this.setState({
            players: players,
            paused: players.some(player => !player.connected),
        });
    }

    startGame() {
        if (this.canStart()) {
            this.state.webSocket.send(JSON.stringify({
                type: RequestTypeEnum.START_GAME,
                game_id: this.state.gameId,
            }));
        }
    }

    getSkulls() {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.GET_SKULLS,
            game_id: this.state.gameId,
        }));
    }

    getResults() {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.GET_RESULTS,
            game_id: this.state.gameId,
        }));
    }

    getConstraints() {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.GET_CONSTRAINTS,
            game_id: this.state.gameId,
        }));
    }
    
    sendPlayer(playerName) {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.REGISTER_PLAYER,
            game_id: this.state.gameId,
            name: playerName
        }));
    }

    sendReconnect(playerName) {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.RECONNECT_PLAYER,
            game_id: this.state.gameId,
            name: playerName
        }));
    }
    
    sendWord(word) {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.ADD_WORD,
            game_id: this.state.gameId,
            word: word
        }))
    }

    sendGuesses(guesses) {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.SEND_GUESSES,
            game_id: this.state.gameId,
            guesses: guesses
        }))
    }

    sendReset() {
        this.setState({resetting: true});
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.RESET,
            game_id: this.state.gameId
        }));
    }

    sendConstraintAmount(amount) {
        this.state.webSocket.send(JSON.stringify({
            type: RequestTypeEnum.SET_CONSTRAINT_AMOUNT,
            amount: amount,
        }));
    }

    askNewCharacter() {
        if (this.state.game.state === GameStateEnum.FST_WORD && !this.state.ack) {
            this.state.webSocket.send(JSON.stringify({
                type: RequestTypeEnum.PICK_NEW_CHARACTER,
            }));
        }
    }
    
    canStart() {
        let playerCount = this.state.players.length;
        return playerCount >= 4 && playerCount <= 8;
    }

    render() {
        return (
            <div className="Game">
                <audio id="notification">
                    <source src="/media/sound/notification.mp3"/>
                </audio>
                <audio id="notification-low">
                    <source src="/media/sound/notification-low.mp3"/>
                </audio>
                {
                    !this.state.registered &&
                    <PlayerRegistration try={this.state.registerTry} started={this.state.started} players={this.state.players} sendPlayer={this.sendPlayer} sendReconnect={this.sendReconnect} />
                }
                {
                    this.state.registered &&
                    !this.state.started &&
                    <Lobby startGame={this.startGame} players={this.state.players} canStart={this.canStart} sendConstraintAmount={this.sendConstraintAmount} constraintAmount={this.state.constraintAmount} />
                }
                {
                    this.state.registered &&
                    this.state.started &&
                    !this.state.resetting &&
                    <div>
                        <Settings colorBlind={this.state.colorBlind} editColorBlind={this.editColorBlind} />
                        <Board players={this.state.players}
                               game={this.state.game}
                               sendWord={this.sendWord}
                               prevWord={this.state.prevWord}
                               ack={this.state.ack}
                               sendGuesses={this.sendGuesses}
                               characters={this.state.characters}
                               guessWords={this.state.guessWords}
                               results={this.state.results}
                               paused={this.state.paused}
                               constraints={this.state.constraints}
                               askNewCharacter={this.askNewCharacter} />
                        <div style={{height: "50px"}}/>
                        <Grid container justify="center">
                            <Grid item xs={4}>
                                <Button variant="contained" color="primary" onClick={this.sendReset}>Recommencer une partie</Button>
                            </Grid>
                        </Grid>
                    </div>
                }
                {
                    this.state.registered &&
                    this.state.started &&
                    this.state.resetting &&
                    <Grid style={{width: "50%"}} container justify="center" spacing={1} className="ResettingPanel">
                        <Grid item xs={7} style={{height: "50px"}}/>
                        <Grid item xs={7}>
                            <span>Lancement de la partie...</span>
                        </Grid>
                        <Grid item xs={7} style={{height: "50px"}}/>
                    </Grid>
                }
                {
                    this.state.registered &&
                    !this.state.started &&
                    this.state.resetting &&
                    <Grid style={{width: "50%"}} container justify="center" spacing={1} className="ResettingPanel">
                        <Grid item xs={7} style={{height: "50px"}}/>
                        <Grid item xs={7}>
                            <span>Création d'une nouvelle partie...</span>
                        </Grid>
                        <Grid item xs={7} style={{height: "50px"}}/>
                    </Grid>
                }
                <div style={{height: "50px"}} />
            </div>
        );
    }
}

export default Game;
