import React from "react";
import './GuessSkull.css'
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

class GuessSkull extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            setGuess: props.setGuess,
            anchorEl: null,
            buttonLabel: "Choisir"
        };
        
        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleClick(event) {
        this.setState({anchorEl: event.currentTarget});
    }
    
    handleClose() {
        this.setState({anchorEl: null});
    }
    
    handleSelect(index) {
        this.state.setGuess(index);
        this.setState({buttonLabel: this.props.characters.find(character => character.index === index).name})
        this.handleClose();
    }

    render() {
        return (
            <div className="skull">
                <Grid item xs={12}>
                    <span className={"guess-word"}>
                        {this.props.guessWord}
                    </span>
                </Grid>
                <Grid item xs={12}>
                    <div style={{height: "10px"}}/>
                </Grid>
                <Grid item xs={12}>
                    <Button variant="contained" color="secondary" aria-controls="simple-menu" aria-haspopup="true" onClick={this.handleClick} disabled={this.props.ack}>
                        {this.state.buttonLabel}
                    </Button>
                    <Menu
                        anchorEl={this.state.anchorEl}
                        keepMounted
                        open={Boolean(this.state.anchorEl)}
                        onClose={this.handleClose}>
                        {
                            this.props.characters.map((character, i) =>
                                <MenuItem key={i} onClick={() => this.handleSelect(character.index)}>{character.name}</MenuItem>
                            )
                        }
                    </Menu>
                </Grid>
            </div>
        )
    }
}

export default GuessSkull;