import React from "react";
import './ResultSkull.css'
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {ResultShowModeEnum} from "../../utils/enums.js";

class ResultSkull extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            showMode: ResultShowModeEnum.BASE,
        };

        this.drawBaseMode = this.drawBaseMode.bind(this);
        this.drawWordsMode = this.drawWordsMode.bind(this);
        this.drawGuessesMode = this.drawGuessesMode.bind(this);
        
        this.switchMode = this.switchMode.bind(this);
        this.getScore = this.getScore.bind(this);
    }
    
    switchMode() {
        switch (this.state.showMode) {
            case ResultShowModeEnum.BASE:
                this.setState({showMode: ResultShowModeEnum.WORDS});
                break;
            case ResultShowModeEnum.WORDS:
                this.setState({showMode: ResultShowModeEnum.GUESSES});
                break;
            case ResultShowModeEnum.GUESSES:
                this.setState({showMode: ResultShowModeEnum.BASE});
                break;
        }
    }
    
    getScore() {
        return this.props.result.player_guesses.filter(guess => guess.character_index === this.props.result.index).length;
    }
    
    drawBaseMode() {
        return (
            <div className="skull">
                <Grid item xs={12}>
                    <span className={"character"}>
                        {this.props.result.character}
                    </span>
                </Grid>
                <Grid item xs={12}>
                    <span className={"word"}>
                        {this.props.result.fth_word}
                    </span>
                </Grid>
                <Grid item xs={12}>
                    <div style={{height: "10px"}}/>
                </Grid>
                <Grid item xs={12}>
                    <span className={"score"}>
                        Score : {this.getScore()} / {this.props.players.length}
                    </span>
                </Grid>
                <Grid item xs={12}>
                    <div style={{height: "10px"}}/>
                </Grid>
                <Grid item xs={12}>
                    <Button variant="contained" color="secondary" aria-controls="simple-menu" aria-haspopup="true" onClick={this.switchMode}>
                        {"Mots"}
                    </Button>
                </Grid>
            </div>
        )
    }

    drawWordsMode() {
        let players = this.props.players;
        
        return (
            <div className="skull">
                <Grid item xs={12}>
                    <span className={"character"}>
                        {this.props.result.character}
                    </span>
                </Grid>
                <Grid item xs={12}>
                    <span className={"word"}>
                        {this.props.result.fst_word}
                    </span><br/>
                    <span className={"player-name"}>
                        ({players.find(player => player.index === this.props.result.index).name})
                    </span>
                </Grid>
                <Grid item xs={12}>
                    <div style={{height: "5px"}}/>
                </Grid>
                <Grid item xs={12}>
                    <span className={"word"}>
                        {this.props.result.sec_word}
                    </span><br/>
                    <span className={"player-name"}>
                        ({players.find(player => player.index === ((this.props.result.index - 1 + players.length) % players.length)).name})
                    </span>
                </Grid>
                <Grid item xs={12}>
                    <div style={{height: "5px"}}/>
                </Grid>
                <Grid item xs={12}>
                    <span className={"word"}>
                        {this.props.result.trd_word}
                    </span><br/>
                    <span className={"player-name"}>
                        ({players.find(player => player.index === ((this.props.result.index - 2 + players.length) % players.length)).name})
                    </span>
                </Grid>
                <Grid item xs={12}>
                    <div style={{height: "5px"}}/>
                </Grid>
                <Grid item xs={12}>
                    <span className={"word"}>
                        {this.props.result.fth_word}
                    </span><br/>
                    <span className={"player-name"}>
                        ({players.find(player => player.index === ((this.props.result.index - 3 + players.length) % players.length)).name})
                    </span>
                </Grid>
                <Grid item xs={12}>
                    <div style={{height: "10px"}}/>
                </Grid>
                <Grid item xs={12}>
                    <Button variant="contained" color="secondary" aria-controls="simple-menu" aria-haspopup="true" onClick={this.switchMode}>
                        {"Propositions"}
                    </Button>
                </Grid>
            </div>
        )
    }

    drawGuessesMode() {
        let players = this.props.players;
        console.log(this.props.result);
        
        return (
            <div className="skull">
                <Grid item xs={12}>
                        <span className={"character"}>
                            {this.props.result.character}
                        </span>
                </Grid>
                <Grid item xs={12}>
                        <span className={"word"}>
                            {this.props.result.fth_word}
                        </span>
                </Grid>
                <Grid item xs={12}>
                    {
                        this.props.result.player_guesses.map((guess, i) =>
                            <Grid key={i} item xs={12}>
                                <span className={"word " + (guess.character_index === this.props.result.index ? "valid" : "invalid")}>
                                    {players.find(player => player.index === guess.player_index).name}
                                </span><br/>
                                <span className={"player-name"}>
                                    {this.props.characters.find(character => character.index === guess.character_index).character}
                                </span>
                            </Grid>
                        )
                    }
                </Grid>
                <Grid item xs={12}>
                    <div style={{height: "10px"}}/>
                </Grid>
                <Grid item xs={12}>
                    <Button variant="contained" color="secondary" aria-controls="simple-menu" aria-haspopup="true" onClick={this.switchMode}>
                        {"Base"}
                    </Button>
                </Grid>
            </div>
        )
    }

    render() {
        switch (this.state.showMode) {
            case ResultShowModeEnum.BASE:
                return this.drawBaseMode();
            case ResultShowModeEnum.WORDS:
                return this.drawWordsMode();
            case ResultShowModeEnum.GUESSES:
                return this.drawGuessesMode();
        }
    }
}

export default ResultSkull;