﻿import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import {Button} from "@material-ui/core";
import "./Lobby.css"
import PlayerList from "./PlayerList";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";

class Lobby extends Component {
    constructor(props) {
        super(props);
        
        this.handleClick = this.handleClick.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        
        this.state = {
            startGame: props.startGame,
            canStart: props.canStart,
            sendConstraintAmount: props.sendConstraintAmount,
            anchorEl: null,
            constraintAmount: props.constraintAmount,
        };
    }

    componentDidUpdate(prevProps) {
        if (this.props.constraintAmount !== prevProps.constraintAmount) {
            this.setState({constraintAmount: this.props.constraintAmount});
        }
    }

    handleClick(event) {
        this.setState({anchorEl: event.currentTarget});
    }

    handleClose() {
        this.setState({anchorEl: null});
    }

    handleSelect(amount) {
        this.state.sendConstraintAmount(amount);
        this.setState({constraintAmount: amount})
        this.handleClose();
    }

    render() {
        let connectedPlayers = this.props.players.filter(player => player.connected);
        return (
            <Grid container justify="center" spacing={3}>
                <Grid item xs={8}>
                    <Grid container style={{width: "50%"}} justify="center" spacing={1} className="LobbyForm">
                        <Grid item xs={7}>
                            <h2>Partie en attente</h2>
                        </Grid>
                        <Grid item xs={7}>
                            <span>{connectedPlayers.length} joueurs connectés</span>
                        </Grid>
                        <Grid item xs={7}>
                            {
                                connectedPlayers.length < 4 &&
                                <span style={{color: "red"}}>
                                    {4 - connectedPlayers.length} joueurs manquants pour commencer
                                </span>
                            }
                            {
                                this.state.canStart() &&
                                <span style={{color: "green"}}>
                                    Il reste de la place pour {8 - connectedPlayers.length} joueurs
                                </span>
                            }
                            {
                                connectedPlayers.length > 8 &&
                                <span style={{color: "red"}}>
                                    Il y a {connectedPlayers.length - 8} joueurs en trop
                                </span>
                            }
                        </Grid>
                        <Grid item xs={12}>
                            <span>
                                Nombre de contraintes :
                            </span><br/>
                            <Button variant="contained" color="secondary" aria-controls="simple-menu" aria-haspopup="true" onClick={this.handleClick}>
                                {this.state.constraintAmount}
                            </Button>
                            <Menu anchorEl={this.state.anchorEl}
                                  keepMounted
                                  open={Boolean(this.state.anchorEl)}
                                  onClose={this.handleClose}>
                                  {
                                      [...Array(5).keys()].map(amount =>
                                          <MenuItem key={amount} onClick={() => this.handleSelect(amount)}>{amount}</MenuItem>
                                      )
                                  }
                            </Menu>
                        </Grid>
                        <Grid item xs={7}>
                            <Button onClick={this.state.startGame} variant="contained" color="primary" disabled={!this.state.canStart()}>
                                Lancer la partie
                            </Button>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={8}>
                    <PlayerList players={connectedPlayers}/>
                </Grid>
            </Grid>
        );
    }
}

export default Lobby;