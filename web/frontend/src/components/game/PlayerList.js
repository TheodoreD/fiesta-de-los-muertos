﻿import React, {Component} from "react";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";

import "./PlayerList.css"

class PlayerList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const players = this.props.players.filter(player => player.connected);
        let rows = [players.slice(0, Math.min(players.length, 4))];
        if (players.length > 4) {
            rows.push(players.slice(4, players.length));
        }
        
        return (
                <div className={"PlayerList"} >
                    <h2>Joueurs</h2>
                    <TableContainer component={"span"}>
                        <Table aria-label="players">
                            <TableBody>
                                {
                                    rows.map((row, i) =>
                                        <TableRow key={i}>
                                            {
                                                rows[i].map((player, j) =>
                                                    <TableCell key={j} align="center" className={(player.hasFinished ? "blue" : "red") + "-cell" + (this.props.colorBlind ? "-cb" : "") + " cell"}>
                                                        {player.name + (player.name === this.props.playerName ? " (me)" : "")}
                                                    </TableCell>
                                                )
                                            }
                                        </TableRow>
                                    )
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
        );
    }
}

export default PlayerList;