﻿import React, {Component} from 'react';
import './GameList.css';
import {Button} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TableContainer from "@material-ui/core/TableContainer";
import TableBody from "@material-ui/core/TableBody";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";

class GameList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            games: []
        };
    }

    componentDidMount() {
        fetch("api/publicGames?format=json",  {
            method: 'GET',
        }).then(response => {
            return response.json();
        }).then(data => {
            this.setState({
                games: data
            });
        });
    }

    render() {
        return (
            <Grid style={{width:"50%"}} container justify="center" spacing={1} className="GameList">
                <Grid item xs={7}>
                    <h2>Parties ouvertes :</h2>
                </Grid>
                <Grid item xs={10}>
                    <TableContainer component={"span"}>
                        <Table aria-label="games">
                            <TableBody>
                                {
                                    this.state.games.map((game, i) =>
                                        <TableRow key={i}>
                                            <TableCell align="right" className="GameName">
                                                {game.game_id}
                                            </TableCell>
                                            <TableCell align="center" className="PlayersAmount">
                                                {game.players_amount === -1 ? 0 : game.players_amount} joueur{game.players_amount !== 1 ? "s" : ""}
                                            </TableCell>
                                            <TableCell align="center" className="JoinGame">
                                                <Button href={'/game?game_id=' + game.game_id} variant="contained" color="primary">
                                                    Rejoindre
                                                </Button>
                                            </TableCell>
                                        </TableRow>
                                    )
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
        );
    }
}

export default GameList;
