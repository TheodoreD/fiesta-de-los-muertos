import React, {Component} from 'react';
import './CreateGameForm.css';
import {Input, Button} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";

class CreateGameForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            invalid: false,
            creating: false,
            created: false,
            existed: false,
            id: "",
            private: ""
        };

        this.createGame = this.createGame.bind(this);
        this.created = this.created.bind(this);
    }

    componentDidMount() {
    }
    
    checkName(name) {
        return name.length > 0 && name.length <= 32 && name.match(/^[a-zA-Z0-9]+$/)
    }

    createGame(id) {
        this.setState({invalid: false});
        if (!this.checkName(id)) {
            this.setState({invalid: true, creating: false});
            return;
        }
        
        fetch("api/createGame?",  {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                game_id: id,
                private: this.state.private
            })
        }).then(response => {
            if (response.status >= 400) {
                this.setState({ // Invalid name
                    invalid: true,
                    creating: false,
                    created: false,
                    id: ""
                });
            }
            else {
                this.setState({
                    existed: response.status === 200,
                    creating: false,
                    created: true,
                    id: id
                });
            }
        });
    }
    
    created() {
        return this.state.created && this.state.id !== "";
    }

    render() {
        if (this.state.creating) {
            return (
                <Grid style={{width:"50%"}} container justify="center" spacing={1} className="CreateGameForm">
                    <Grid item xs={7} style={{height: "50px"}} />
                    <Grid item xs={7}>
                        <span>Création...</span>
                    </Grid>
                    <Grid item xs={7} style={{height: "50px"}} />
                </Grid>
            );
        }
        
        return (
            <Grid style={{width:"50%"}} container justify="center" spacing={1} className="CreateGameForm">
                <Grid item xs={7}>
                    <h1 className={"title"}>
                        Fiesta De Los Muertos
                    </h1>
                    <h4 className={"subTitle"}>
                        aka Le Jeu De Ses Morts
                    </h4>
                </Grid>
                <Grid item xs={7}>
                    {
                        !this.created() &&
                        <h2>
                            Créer / rejoindre une partie :
                        </h2>
                    }
                </Grid>
                <Grid item xs={7}>
                    {this.state.invalid && <span style={{color: "red"}}>Erreur : le nom de la partie doit être un seul mot avec uniquement des chiffres et des lettres</span>}
                </Grid>
                <Grid item xs={7}>
                    {
                        this.created() &&
                        !this.state.existed &&
                        <span>La partie {this.state.id} a été créée !</span>
                    }
                    {
                        this.created() &&
                        this.state.existed &&
                        <span>La partie {this.state.id} existe déjà !</span>
                    }
                    {
                        !this.created() &&
                        <Input fullWidth onChange={(event) => this.setState({id: event.target.value})} />
                    }
                </Grid>
                <Grid item xs={7}>
                    {
                        !this.created() &&
                        <FormControlLabel control={
                            <Checkbox onChange={(event) => {this.setState({private: event.target.checked})}}
                                      name="private"
                                      color="primary" />
                        } label="Partie privée" />
                    }
                </Grid>
                <Grid item xs={7}>
                    {
                        !this.created() &&
                        <Button onClick={() => {this.setState({creating: true}); this.createGame(this.state.id);}} variant="contained" color="primary">
                            Creer une partie
                        </Button>
                    }
                    {
                        this.created() &&
                        <Button href={'/game?game_id=' + this.state.id} variant="contained" color="primary">
                            Rejoindre la partie
                        </Button>
                    }
                </Grid>
            </Grid>
        );
    }
}

export default CreateGameForm;
