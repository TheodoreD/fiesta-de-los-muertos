﻿export let RequestTypeEnum = {
    RESET: 1,
    REGISTER_PLAYER: 2,
    GET_PLAYERS: 3,
    REMOVE_PLAYER: 4,
    GET_SKULLS: 5,
    ADD_WORD: 6,
    SEND_GUESSES: 7,
    START_GAME: 8,
    GET_RESULTS: 9,
    RECONNECT_PLAYER: 10,
    GET_CONSTRAINTS: 11,
    SET_CONSTRAINT_AMOUNT: 12,
    PICK_NEW_CHARACTER: 13,
};

export let GameStateEnum = {
    FST_WORD: 0,
    SEC_WORD: 1,
    TRD_WORD: 2,
    FTH_WORD: 3,
    GUESSES: 4,
    FINISHED: 5,
    LOBBY: 6,
};

export let ResultShowModeEnum = {
    BASE: 0,
    WORDS: 1,
    GUESSES: 2,
};