FROM python:3.6-alpine

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=fiesta.settings_dev

RUN mkdir /fiesta

WORKDIR /fiesta

RUN apk add --update nodejs npm alpine-sdk libffi-dev libressl-dev

ADD web/requirements.txt /fiesta/requirements.txt

RUN pip install --upgrade pip && pip install -r requirements.txt

WORKDIR /fiesta/frontend

ADD web/frontend/package.json /fiesta/frontend/package.json
ADD web/frontend/.babelrc /fiesta/frontend/.babelrc
ADD web/frontend/webpack.config.js /fiesta/frontend/webpack.config.js

RUN npm install

ADD web/frontend/ /fiesta/frontend/

RUN npm run dev

ADD web/ /fiesta/

WORKDIR /fiesta

CMD python manage.py migrate && \
    python populate_database.py characters constraints 1 && \
    python manage.py runserver 0.0.0.0:65000
