FROM python:3.6-alpine

ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE=fiesta.settings_prod_postgres

ENV FIESTA_HOST=localhost
ENV REDIS_HOST=redis
ENV REDIS_PORT=6379
ENV POSTGRES_HOST=postgres
ENV POSTGRES_PORT=""
ENV POSTGRES_DB=fiesta
ENV POSTGRES_USER=user
ENV POSTGRES_PASSWORD=secret

RUN mkdir /fiesta

WORKDIR /fiesta

RUN apk add --update nodejs        \
                     npm           \
                     alpine-sdk    \
                     libffi-dev    \
                     libressl-dev  \
                     postgresql-dev

RUN pip install psycopg2

ADD web/requirements.txt /fiesta/requirements.txt

RUN pip install --upgrade pip && pip install -r requirements.txt

WORKDIR /fiesta/frontend

ADD web/frontend/package.json /fiesta/frontend/package.json
ADD web/frontend/.babelrc /fiesta/frontend/.babelrc
ADD web/frontend/webpack.config.js /fiesta/frontend/webpack.config.js

RUN npm install

ADD web/frontend/ /fiesta/frontend/

RUN npm run build

RUN rm -rf node_modules src

ADD web/ /fiesta/

WORKDIR /fiesta

RUN python manage.py collectstatic

CMD python manage.py migrate && \
    python populate_database.py characters constraints 2 && \
    python manage.py runserver 0.0.0.0:65000
